Wordpress-Bedrock-Theme
=======================

Version 1.0:

+ Initial Release

Version 1.1:

+ Register parent theme template in samplechildtheme (doh!)
+ Updated all template tag names
+ Page layouts aren't broken by default