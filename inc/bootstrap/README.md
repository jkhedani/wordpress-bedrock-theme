
================================================

DCDC Starter Template 
Version: 1.0
Authors: Justin Hedani, DCDC
Author URL: hedanidesign.com
Notes: Based on the Twitter Bootstrap Framework - http://twitter.github.com/bootstrap/

================================================

DOCUMENTATION

Table of Contents
1. Overview
2. How To Use This Bootstrap
3. Working with git/github
4. A new section for a new future

============================
1. Overview
============================

The ultimate set of tools to build your ultimately awesome website!

============================
2. How To Use This Bootstrap
============================

Why, put it on your boot of course! ... I'm so sorry for that.

To use this bootstrap in ANY site wether it's a standalone HTML
site or a Wordpress build, all you need to do is:

1. Copy over the three folders: "css", "img" and "js" to you site's root directory.
2. For basic sites, just include 'index.html'. If you prefer a more complex option,
	 choose one of our many HTML/PHP templates we've made.

============================
3. Working with git/github
============================
